const booksByCategory = [
  {
    category: "Riqueza",
    books: [
      {
        title: "Os segredos da mente milionária",
        author: "T. Harv Eker",
      },
      {
        title: "O homem mais rico da Babilônia",
        author: "George S. Clason",
      },
      {
        title: "Pai rico, pai pobre",
        author: "Robert T. Kiyosaki e Sharon L. Lechter",
      },
    ],
  },
  {
    category: "Inteligência Emocional",
    books: [
      {
        title: "Você é Insubstituível",
        author: "Augusto Cury",
      },
      {
        title: "Ansiedade – Como enfrentar o mal do século",
        author: "Augusto Cury",
      },
      {
        title: "Os 7 hábitos das pessoas altamente eficazes",
        author: "Stephen R. Covey"
      }
    ]
  }
];

console.log("Ao todo temos " + booksByCategory.length + " categorias.")

for (var count = 0; count < booksByCategory.length; count++) {
  console.log("A categoria " + booksByCategory[count].category + " possui " + booksByCategory[count].books.length + " livros.")
}




var autores = []

function repeat(a) {
  return autores.find(element => element == a) == a
}

for (var count2 = 0; count2 < booksByCategory.length; count2++) {
  for (var count3 = 0; count3 < booksByCategory[count2].books.length; count3++) {
    if (repeat(booksByCategory[count2].books[count3].author) != true) {
      autores.push(booksByCategory[count2].books[count3].author)
    }
  }
}

console.log("Ao todo são " + autores.length + " autores.")



var author = prompt("Digite o nome do autor para saber os livros:")

var booksForAutor = []

for (var count2 = 0; count2 < booksByCategory.length; count2++) {
  for (var count3 = 0; count3 < booksByCategory[count2].books.length; count3++) {
    if (booksByCategory[count2].books[count3].author == author) {
      booksForAutor.push(booksByCategory[count2].books[count3].title)
    }
  }
}

console.log("Os livros do autor "+ author +": " + booksForAutor)
