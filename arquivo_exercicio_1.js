function multiply(a, b) {
  var aNumRows = a.length
  var aNumCols = a[0].length
  var bNumRows = b.length 
  var bNumCols = b[0].length
  var m = new Array(aNumRows)
  for (var r = 0; r < aNumRows; ++r) {
    m[r] = new Array(bNumCols); 
    for (var c = 0; c < bNumCols; ++c) {
      m[r][c] = 0;             
      for (var i = 0; i < aNumCols; ++i) {
        m[r][c] += a[r][i] * b[i][c]
      }
    }
  }
  return m
}

function display(m) {
  for (var r = 0; r < m.length; ++r) {
    document.write('  '+m[r].join(' ')+'<br />')
  }
}

var a =  [ [ [2],[-1] ], [ [2],[0] ] ]

var b =   [ [2,3],[-2,1] ]

var c = [ [4,0], [-1,-1] ]

var d =   [ [-1,3], [2,7] ]

console.log(multiply(a, b))

console.log(multiply(c, d))



